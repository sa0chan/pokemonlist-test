import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TranslatorComponent } from './translator/translator.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {funTranslatorService} from './service/funTranslator.service';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';

@NgModule({
  declarations: [
    AppComponent,
    TranslatorComponent,
    ListPokemonComponent
  ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
    ],
  providers: [funTranslatorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
