import { Component, OnInit } from '@angular/core';
import {funTranslatorService} from '../service/funTranslator.service';
import {PokemonModel} from '../model/pokemon.model';

@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.css']
})

export class ListPokemonComponent implements OnInit {

  listPokemon: PokemonModel[] = [];

  constructor(private service: funTranslatorService) {
    console.log(this.service.getTranslate().subscribe(response => {
      response.forEach(pokemon => {
        this.listPokemon.push(pokemon);
        console.log(pokemon.name);
      });
    }));


  }

  ngOnInit() {
  }

}
