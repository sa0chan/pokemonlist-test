import {PokemonModel} from './pokemon.model';

export class ListPokemonModel{
  // tslint:disable-next-line:no-unused-expression
  results: PokemonModel[];

  constructor(listPokemon) {

    this.results = listPokemon;
  }
}
