export class TranslatorModel {

  language: string ;
  inputToTranslate: string;

  constructor(language: string, inputToTranslate: string) {
    this.language = language;
    this.inputToTranslate = inputToTranslate;

  }


}
