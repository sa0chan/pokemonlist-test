import { Component, OnInit } from '@angular/core';
import {funTranslatorService} from '../service/funTranslator.service';

@Component({
  selector: 'app-translator',
  templateUrl: './translator.component.html',
  styleUrls: ['./translator.component.css']
})
export class TranslatorComponent implements OnInit {

  languageValue: string;
  inputValue = '';
  finalValue = '';

  getInputValue(reponse: string) {
    this.inputValue = reponse;
    console.log(this.inputValue);

  }

  getLanguageValue(choice) {
    this.languageValue =  choice ;
  }

  enregistrer() {
    this.finalValue = this.inputValue;
    return this.finalValue;
  }

  constructor(private service: funTranslatorService) {

    // ecoute l'api
    console.log(this.service.getTranslate().subscribe());
  }

  ngOnInit() {
  }

}
